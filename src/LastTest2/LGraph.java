package LastTest2;

import java.util.Scanner;

public class LGraph {
    int vertexNum;
    int edgeNum;
    Edge[] edgeslist;
    Scanner scanner=new Scanner(System.in);

    public  LGraph(int vertexNum,int edgeNum){
        this.vertexNum=vertexNum;
        this.edgeNum=edgeNum;
    }
    // 图的构造
    public void createGrade(){
        Edge[] edgeslist=new Edge[edgeNum-1];
        this.edgeslist=edgeslist;
        for(int i=0;i<edgeNum;i++){
            System.out.println("输入第"+i+1+"条边的头和尾：");
            int a=scanner.nextInt();
            edgeslist[i].getper(a);
            int b=scanner.nextInt();
            edgeslist[i].getfol(b);
        }
        // 邻接表表示
        for(int j=0;j<vertexNum;j++){
            System.out.print(j);
            for(int i=0;i<edgeNum;i++){
                if(edgeslist[i].predot==j){
                    System.out.print(" => "+ edgeslist[i].foldot);
                }
                System.out.println();
            }
        }
    }
    // 求出入度
    public void getInOut(int dotNum){
        int indu=0;
        int outdu=0;
        for (int i=0;i<edgeNum;i++){
            if(edgeslist[i].predot==dotNum){
                outdu++;
            }
            if(edgeslist[i].foldot==dotNum){
                indu++;
            }
        }
        System.out.println("点"+dotNum+"入度为"+indu+"出度为"+outdu);
    }
    // 拓扑

}

