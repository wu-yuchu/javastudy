package Graph;

import java.util.Scanner;

public class Graph {

    int verNum;

    int edgeNum;

    Vertex[] verArray;

    public Graph() {
        Scanner scan = new Scanner(System.in);
        System.out.println("请选择你想要构建有向图（2）还是无向图（1）： ");
        int choose = scan.nextInt();
        System.out.println("请输入节点个数和边的个数：");
        verNum = scan.nextInt();
        edgeNum = scan.nextInt();
        verArray = new Vertex[verNum];

        System.out.println("请依次输入节点的名称:");
        for (int i=0;i<verNum;i++){
            Vertex vertexmem = new Vertex();
            vertexmem.verName = scan.next();
            vertexmem.edgeLink = null;
            verArray[i] = vertexmem;
        }

        System.out.println("请按‘头节点 尾节点 回车’的形式依次输入边的信息");
        for (int i=0;i<edgeNum;i++){
            String preName = scan.next();
            String folName = scan.next();

            Vertex preV = getVertex(preName);
            Vertex folV = getVertex(folName);
            if (preV == null || folV == null){
                System.out.println("输入错误，输入了不存在的顶点！请重新输入");
                i--;
                continue;
            }

            Edge edge = new Edge();
            edge.tailName = folName;

            edge.broEdge = preV.edgeLink;
            preV.edgeLink = edge;
            if(choose==1){
                Edge edgeelse = new Edge();
                edgeelse.tailName = preName;
                edgeelse.broEdge  = folV.edgeLink;
                folV.edgeLink = edgeelse;}

        }
    }


    public Vertex getVertex(String verName){
        for (int i=0;i<verNum;i++){
            if (verArray[i].verName.equals(verName))
                return verArray[i];
        }
        return null;
    }
}
