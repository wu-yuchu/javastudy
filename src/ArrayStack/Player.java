package ArrayStack;

public class Player {
    private String name;
    private String team;
    private String country;
    private int top;
    public Player(){
        this.name=name;
        this.team=team;
        this.country = country;
        this.top=top;
    }

    public String getName(){
        return name;
    }
    public  void  setName(){
        this.name=name;
    }
    public String getTeam(){
        return  team;
    }
    public void setTeam(){
        this.team=team;
    }
    public String getCountry(){
        return  country;
    }
    public void setCountry(){
        this.country=country;
    }
    public int getTop(){
        return  top;
    }
    public void setTop(){
        this.top=top;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", team='" + team + '\'' +
                ", country='" + country + '\'' +
                ", top" + top +
                '}';
    }
}
