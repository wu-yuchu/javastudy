package ArrayStack;

import java.util.Arrays;

public class ArrayStack<T> implements StackADT {
    private final int CAPACITY = 4;
    private int top ;
    private T[] stack;

    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[CAPACITY]);
    }

    public ArrayStack(int initialCapacity){
        top=0;
        stack=(T[]) (new Object[initialCapacity]);
    }

    public void push(Object element){
        if (size() == stack.length)
            expandCapacity();
        stack[top] = (T) element;
        top++;
    }



    public T pop() {
        if (isEmpty())
            System.out.println("Stack");
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }

    public T peek(){
        if (isEmpty())
            System.out.println("Stack");
        return stack[top-1];
    }

    public boolean isEmpty() {
        if(stack[top] == null && top == 0)
            return true;
        else
            return false;
    }

    public int size() {
        return 0;
    }

    public String toString() {
        return "ArrayStack:" +
                "arrayStack2321=" + Arrays.toString(stack) +
                ", top=" + top +
                '.';
    }
    public void expandCapacity() {
        T[] larger = (T[]) (new Object[stack.length*2]);
        for(int index=0;index<stack.length;index++)
            larger[index] = stack[index];
        stack=larger;
    }


}

