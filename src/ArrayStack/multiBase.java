package ArrayStack;

import java.util.Scanner;
import java.util.Arrays;

public class multiBase {
    public static void multiBase(int N, int B){
        int i;
        ArrayStack s = new ArrayStack();
        while (N!=0){
            s.push(N%B);
            N=N/B;
        }
        while (!s.isEmpty()){

            System.out.print(s.pop());
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入一个十进制数：");
        int n = sc.nextInt();
        System.out.println("输入进制：");
        int b = sc.nextInt();
        multiBase(n,b);
    }

}
