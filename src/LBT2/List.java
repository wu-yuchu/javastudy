package LBT2;

import java.util.Iterator;


public interface List<T> extends Iterable<T>
{

    public T removeFirst() throws EmptyCollectionException;


    public T removeLast() throws EmptyCollectionException;


    public T remove(T element) throws ElementNotFoundException;


    public T first() throws EmptyCollectionException;


    public T last() throws EmptyCollectionException;


    public boolean contains(T target);


    public boolean isEmpty();


    public int size();


    @Override
    public Iterator<T> iterator();


    @Override
    public String toString();
}