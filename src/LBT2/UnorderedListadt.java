package LBT2;
public interface UnorderedListadt<T> extends List<T>
{

    public void addToFront(T element);

    public void addToRear(T element);

    public void addAfter(T element, T target);
}