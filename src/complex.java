public class complex {
    double RealPart;
    double ImagePart;
    public complex(double RealPart,double ImagePart){
        this.RealPart=RealPart;
        this.ImagePart=ImagePart;
    }
    public static double getRealPart(double RealPart){
        return RealPart;
    }
    public double getImagePart(double ImagePart){
        return ImagePart;
    }
    public complex complexAdd(complex a){
        return new complex(RealPart+a.RealPart,ImagePart+a.ImagePart);
    }
    public complex complexSub(complex a){
        return new complex(RealPart-a.RealPart,ImagePart-a.ImagePart);
    }
    public complex complexDiv(complex a){
        return new complex((RealPart*a.RealPart+ImagePart*a.ImagePart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart),(ImagePart*a.ImagePart-RealPart*a.RealPart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart));
    }
    public complex complexMul(complex a){
        return new complex(RealPart*a.RealPart-ImagePart*a.ImagePart,ImagePart*a.RealPart+RealPart*a.ImagePart);
    }
    public String toString(){
        String s=" ";
        if(ImagePart>0)
            s=RealPart+"+"+ImagePart+"i";
        if(ImagePart==0)
            s=RealPart+" ";
        if(ImagePart<0)
            s=RealPart+""+ImagePart+"i";
        return s;

    }

}