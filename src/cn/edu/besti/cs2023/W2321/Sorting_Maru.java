package cn.edu.besti.cs2023.W2321;
import java.util.Arrays;
public class Sorting_Maru  {
 /*   private int[] data;
    public void getData(int[] data){
        this.data=data;
    }*/
    int []data;
    int top;
    public int[] SelectionSortUp(int[] data){
        int min;
        for (int i=0;i<data.length-1;i++){
            min=i;
            for (int j=i+1;j<data.length;j++){
                if (data[j]<data[min])
                    min=j;
            }
            if(min!=i)
            swap(data,min,i);
        }
        return data;

    }
    public int[] SelectionSortDown(int[] data){
        int max;
        for (int i=0;i<data.length-1;i++){
            max=i;
            for (int j=i+1;j<data.length;j++){
                if (data[j]>data[max])
                    max=j;
            }
            if(max!=i)
            swap(data,max,i);
        }
        return data;

    }
    public void swap(int[] data,int indexA,int indexB){
        int temp=data[indexA];
        data[indexA]=data[indexB];
        data[indexB]=temp;
    }
    public void outPut(int[] data){
        for(int i=0;i<data.length;i++)
        {
            System.out.print(data[i]);
            if(i<data.length-1){
                System.out.print(",");
            }
            else if(i==data.length-1){
                System.out.println("");
            }
        }

    }


}
