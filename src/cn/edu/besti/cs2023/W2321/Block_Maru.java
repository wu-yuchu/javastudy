package cn.edu.besti.cs2023.W2321;

public class Block_Maru {
    private int data[];
    private int length;
    public boolean BlockSearch(Block_Maru bm,int[][] blockTable,int bmlength,int btlength,int target){
        int low=0,high=btlength-1,mid;
        BlockTable bt=bm.new BlockTable();
        bt.createBlockTable(blockTable);
        while (low<=high){
            mid=(low+high)/2;
            if (bt.node[mid].top>=target){
                high=mid-1;
            }
            else
                low=mid+1;
        }
        if (low<btlength){
            int i=bt.node[low].start;
            while (i<=bt.node[low].start+bmlength/btlength-1&&bm.data[i]!=target){
                i++;
            }
            if (i<=bt.node[low].start+bmlength/btlength-1){
                return true;
            }else
                return false;

        }
        return false;
    }
    public void createBlock(int[] data){
        this.data=new int[data.length];
        for (int i=0;i<data.length;i++){
            this.data[i]=data[i];
            this.length++;
        }
    }
    public class Node{
        public int top;
        public int start;
    }
    public class BlockTable{
        public Node[] node;
        public int length=0;
        public void createBlockTable(int data[][]){
            this.node=new  Node[data.length];
            for (int i=0;i<data.length;i++){
                node[i]=new Node();
                node[i].top=data[i][0];
                node[i].start=data[i][1];
                this.length++;
            }
        }
    }
}
