package Linked;

public interface StackADT {
    public int getSize();

    public void add();

    public void delete();

    public boolean isEmpty();

    public boolean isInclude();

    public boolean toCompare();

    public void rankCompare();

}
