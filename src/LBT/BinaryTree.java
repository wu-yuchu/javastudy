package LBT;

import java.util.Iterator;

public interface BinaryTree<T> {

    public T getRootElement() throws  Exception;

    public BinaryTree<T> getLeft() throws  Exception;

    public BinaryTree<T> getRight() throws Exception;


    public boolean contains (T target) throws Exception;

    public T find (T target) throws  Exception;

    public boolean isEmpty();

    public int size();
    public String toString();
    public Iterator<T> preorder();
    public Iterator<T> inorder();
    public Iterator<T> postorder();
    public Iterator<T> levelorder() throws Exception;

    Iterator<T> iterator();
}
