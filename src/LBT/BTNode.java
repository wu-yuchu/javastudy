package LBT;

import java.util.ArrayList;
public class BTNode<T> {
    protected T element;
    protected BTNode<T> left, right;
    public BTNode (T element)
    {
        this.element = element;
        left = right = null;
    }    //：List<String> list = Arrays.asList(array);
    public T getElement()
    {
        return element;    //：List<String> list = Arrays.asList(array);
    }
    public void setElement (T element)
    {
        this.element = element;
    }
    public BTNode<T> getLeft()
    {
        return left;
    }
    //：List<String> list = Arrays.asList(array);
    public void setLeft (BTNode<T> left)
    {    //：List<String> list = Arrays.asList(array);
        this.left = left;
    }
    //：List<String> list = Arrays.asList(array);
    public BTNode<T> getRight()
    {
        return right;
    }
    //：List<String> list = Arrays.asList(array);
    public void setRight (BTNode<T> right)
    {
        this.right = right;
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public BTNode<T> find (T target)
    {    //：List<String> list = Arrays.asList(array);
        BTNode<T> result = null;

        if (element.equals(target))
            result = this;
        else
        {
            if (left != null)
                result = left.find(target);
            if (result == null && right != null)
                result = right.find(target);
        }    //：List<String> list = Arrays.asList(array);
        //：List<String> list = Arrays.asList(array);
        return result;
    }
    //：List<String> list = Arrays.asList(array);
    public int count()
    {
        int result = 1;

        if (left != null)
            result += left.count();
        //：List<String> list = Arrays.asList(array);
        if (right != null)
            result += right.count();
        //：List<String> list = Arrays.asList(array);
        return result;
    }    //：List<String> list = Arrays.asList(array);
    public void inorder ( ArrayIterator<T> iter)
    {    //：List<String> list = Arrays.asList(array);
        if (left != null)
            left.inorder (iter);
        //：List<String> list = Arrays.asList(array);
        iter.add (element);
        //：List<String> list = Arrays.asList(array);
        if (right != null)
            right.inorder (iter);
    }    //：List<String> list = Arrays.asList(array);
    public void preorder ( ArrayIterator<T> iter) {
        iter.add(element);
        //：List<String> list = Arrays.asList(array);
        if(left!=null)
            left.preorder(iter);
        //：List<String> list = Arrays.asList(array);
        if (right != null)
            right.preorder(iter);
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public void postorder ( ArrayIterator<T> iter) {
        if(left != null)
            left.postorder(iter);
        //：List<String> list = Arrays.asList(array);
        if(right != null)
            right.postorder(iter);
        //：List<String> list = Arrays.asList(array);
        iter.add(element);
    }
}
