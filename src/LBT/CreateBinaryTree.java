package LBT;

import java.util.LinkedList;
public class CreateBinaryTree {

    static BinaryTreeNode root = new BinaryTreeNode();
    static int i =0;
    static LinkedList<BinaryTreeNode> list = new LinkedList<>();

    public static BinaryTreeNode add(char[] ch)//构造二叉树
    {

        BinaryTreeNode node = null;
        if(ch[i]=='*'){
            node = null;
            i++;
        }
        else
        {
            node = new BinaryTreeNode();
            node.element = ch[i];
            i++;
            node.left = add(ch);
            node.right = add(ch);
        }

        return node;
    }

    static public void level(BinaryTreeNode root){
        if(root==null)
        {
            return;
        }
        list.add(root);
        while (!list.isEmpty())
        {
            BinaryTreeNode temp = list.poll();
            System.out.print(temp.element+" ");
            if(temp.left!=null)
                list.add(temp.left);
            if(temp.right!=null)
                list.add(temp.right);
        }
    }
}
