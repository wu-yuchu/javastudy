package LastTest;

public class Student implements ComputeSum{
    private String name;
    private int xh;
    private float javascore;
    private float Englishscore;
    private float mathscore;
    private float allscore;
    public void inputStudent(String name,int xh,float javascore,float Englishscore,float mathscore){
        this.javascore=javascore;
        this.Englishscore=Englishscore;
        this.mathscore=mathscore;
        this.xh=xh;
        this.name=name;
        this.allscore=javascore+mathscore+Englishscore;
    }
    public String printStudent(){
        return "姓名"+name+"学号"+xh+"java成绩"+javascore+"英语成绩"+Englishscore+" 数学成绩"+mathscore+"总分为"+allscore;
    }
    public float allSum(){
        return allscore;
    }
    public float getJavascore(){
        return javascore;
    }


}
