package Searching_and_Sorting;
import cn.edu.besti.cs2023.W2321.Block_Maru;
import cn.edu.besti.cs2023.W2321.HashSearch_Maru;
import cn.edu.besti.cs2023.W2321.Searching_Maru;
import cn.edu.besti.cs2023.W2321.BST_Maru;
import junit.framework.TestCase;
import org.junit.Test;
public class SearchTest extends TestCase{
    Searching_Maru data1=new Searching_Maru();
    /*线性查找*/
    int[] array1={5,8,13,9,20,20,23,21,11,11,10};
    @Test
    public void testlinearSearch()throws Exception{
        assertEquals(true,data1.linearSearchNone(array1,5));//边界
        assertEquals(true,data1.linearSearchNone(array1,13));//正常
        assertEquals(false,data1.linearSearchNone(array1,2321));//异常
    }
    /*二分查找*/
    int[] array2={2,3,5,11,15,20,21,23} ;//按顺序排序
    @Test
    public void testbinarySearch()throws  Exception{
        assertEquals(true,data1.binarySearch(array2,2));//边界
        assertEquals(true,data1.binarySearch(array2,20));//正常
        assertEquals(false,data1.binarySearch(array2,2321));//异常
    }
    /*插值查找*/
    @Test
    public void testinsertionSearch()throws Exception{
        int[] array3=new int[100];
        for (int i=0;i<array3.length-1;i++){
            array3[i]=i+1;
        }
        assertEquals(true,data1.insertionSearch(array3,2,1,10));//边界
        assertEquals(true,data1.insertionSearch(array3,20,10,30));//正常
        assertEquals(false,data1.insertionSearch(array3,2321,1,99));//异常
    }

    /*斐波那契查找*/
    int[] array4={3,20,23,42,86,2321};
    @Test
    public void testFibonacciSearch()throws Exception{
        assertEquals(true,data1.fibonacciSearch(array4,2321));//边界
        assertEquals(true,data1.fibonacciSearch(array4,23));//正常
        assertEquals(false,data1.fibonacciSearch(array4,-1));//异常
    }
    /*树表查找*/
    BST_Maru data2=new BST_Maru();
    int[] array5={5,7,88,67,11,2321,13,114,2021,42};
    @Test
    public void testBST()throws Exception{
       assertEquals(true,data2.isBSTfound(array5,42));//边界
       assertEquals(true,data2.isBSTfound(array5,2321));//正常
       assertEquals(false,data2.isBSTfound(array5,-1));//异常
    }
    /*分块查找*/
    Block_Maru data3=new Block_Maru();
    int[] array6={4, 42, 36, 27, 86, 2321, 6657, 123321, 1, 33, 9, 58,11,12};
    int[][] array6Table={{42,0},{123321,4},{58,9}};
    @Test
    public void testBlockSearch()throws Exception{
        data3.createBlock(array6);
        assertEquals(true,data3.BlockSearch(data3,array6Table,array6.length,array6Table.length,4));//边界
        assertEquals(true,data3.BlockSearch(data3,array6Table,array6.length,array6Table.length,2321));//正常
        assertEquals(false,data3.BlockSearch(data3,array6Table,array6.length,array6Table.length,100));//异常
    }

    /*哈希查找*/
    HashSearch_Maru data4=new HashSearch_Maru();
    int[] array7={23,21,2321,20,11,12,17,34,2020,2023};
    @Test
    public void testHashSearch()throws Exception{
        assertEquals(true,data4.hashSearch(array7,2023));//边界
        assertEquals(true,data4.hashSearch(array7,2321));//正常
        assertEquals(false,data4.hashSearch(array7,100));//异常

    }
}
