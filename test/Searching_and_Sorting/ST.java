package Searching_and_Sorting;
import cn.edu.besti.cs2023.W2321.Searching_Maru;
import cn.edu.besti.cs2023.W2321.Sorting_Maru;
import junit.framework.TestCase;
import org.junit.Test;

public class ST extends TestCase {

    int[] a={0,2,11,17,23,38,56,72,86,91,-1};//0为哨兵，-1为终止位
    int[] aa={2,11,17,23,38,56,72,86,91,2321};
    int[] b={0,3,4,6,8,9,13,-1};//0为哨兵，-1为终止位
    int[] bb={3,4,6,8,9,13,2321};

    Searching_Maru data =new Searching_Maru();
    Sorting_Maru dataA=new Sorting_Maru();
    int[] target={2,91,23,86,1,100};//边界 边界 正常 正常 异常 异常
    int[] targetA={3,13,6,-1};//边界 边界 正常 异常
    @Test
    public void testSearch1() throws Exception{
        assertEquals(1,data.linearSearch(a,target[0]));
        assertEquals(9,data.linearSearch(a,target[1]));
        assertEquals(4,data.linearSearch(a,target[2]));
        assertEquals(8,data.linearSearch(a,target[3]));
        assertEquals(0,data.linearSearch(a,target[4]));
        assertEquals(0,data.linearSearch(a,target[5]));
        assertEquals(1,data.linearSearch(b,targetA[0]));
        assertEquals(6,data.linearSearch(b,targetA[1]));
        assertEquals(3,data.linearSearch(b,targetA[2]));
        assertEquals(0,data.linearSearch(b,targetA[3]));

    }
    @Test
    public void testSearch2() throws Exception{
        assertEquals(true,data.linearSearchNone(aa,target[0]));
        assertEquals(true,data.linearSearchNone(aa,target[1]));
        assertEquals(false,data.linearSearchNone(aa,target[4]));
        assertEquals(true,data.linearSearchNone(bb,targetA[0]));
        assertEquals(true,data.linearSearchNone(bb,targetA[1]));
        assertEquals(false,data.linearSearchNone(bb,targetA[3]));

    }
    @Test
    public void testSort() throws Exception{
        assertEquals(aa,dataA.SelectionSortUp(aa));
        assertEquals(new int[]{2321,91,86,72,56,38,23,17,11,2},dataA.SelectionSortDown(aa));
        assertEquals(bb,dataA.SelectionSortUp(bb));
        assertEquals(new int[]{2321,13,9,8,6,4,3},dataA.SelectionSortDown(bb));
    }



}