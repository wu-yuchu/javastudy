import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("Thisismytestingstring");
    StringBuffer c = new StringBuffer("Thisismytestingstringandihavechangedmuch");
    @Test
    public void testcharAt() throws Exception {
        assertEquals('t', a.charAt(1));
        assertEquals('g', a.charAt(5));
        assertEquals('r', a.charAt(11));
    }
    @Test
    public void testcapacity() throws Exception {
        assertEquals(28, a.capacity());
        assertEquals(37, b.capacity());
        assertEquals(56, c.capacity());
    }
    @Test
    public void testlength() throws Exception {
        assertEquals(12, a.length());
        assertEquals(21, b.length());
        assertEquals(40, c.length());
    }
    @Test
    public void testindexOf() throws Exception {
        assertEquals(0, a.indexOf("Str"));
        assertEquals(6, b.indexOf("my"));
        assertEquals(21, c.indexOf("and"));
    }
}